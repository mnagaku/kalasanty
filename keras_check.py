import sys
import h5py

with h5py.File(sys.argv[1], "r") as fp:
    print(fp.attrs.get("keras_version"))
